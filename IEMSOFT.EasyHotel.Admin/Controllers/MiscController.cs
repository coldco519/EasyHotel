﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Lib;
using IEMSOFT.Foundation;
using IEMSOFT.EasyHotel.Admin.Models;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class MiscController : BaseController
    {
        public ActionResult YearOption()
        {
            var ret = new List<SelectListItem>();
            var startYear = 2014;
            var endYear=DateTime.Now.Year;
            while (startYear <= endYear)
            {
                var tmp = new SelectListItem();
                tmp.Text = startYear.ToString();
                tmp.Value = startYear.ToString();
                if (startYear == endYear)
                {
                    tmp.Selected = true;
                }
                ret.Add(tmp);
                startYear++;
            }
            return JsonNet(ret);
        }

        public ActionResult MonthOption()
        {
            var ret = new List<SelectListItem>();
            var currentMonth = DateTime.Now.Month;
            for (int i =1 ; i <=12; i++)
            {
                var tmp = new SelectListItem();
                tmp.Text = i.ToString();
                tmp.Value = i.ToString();
                if (i == currentMonth)
                {
                    tmp.Selected = true;
                }
                ret.Add(tmp);
            }
            return JsonNet(ret);
        }
        public ActionResult IsHourRoomOption(string defaultText, string defaultValue)
        {
            List<SelectListItem> options = new List<SelectListItem>();
            options.Add(new SelectListItem { Text = "否", Value = "0" });
            options.Insert(1, new SelectListItem { Text = "是", Value = "1" });
            options[0].Selected = true;
            return JsonNet(options);
        }
    }
}
